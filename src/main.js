import Vue from 'vue'
import App from './App'


import '../node_modules/vue-material/dist/vue-material.css'

/*
import VueMaterial from 'vue-material'
Vue.use(VueMaterial);
*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});
